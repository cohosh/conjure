module git.torproject.org/pluggable-transports/conjure

go 1.17

require (
	git.torproject.org/pluggable-transports/goptlib.git v1.2.0
	git.torproject.org/pluggable-transports/snowflake.git/v2 v2.2.0
	github.com/pires/go-proxyproto v0.6.2
	github.com/refraction-networking/gotapdance v1.3.0
)

require (
	filippo.io/edwards25519 v1.0.0-rc.1.0.20210721174708-390f27c3be20 // indirect
	github.com/dchest/siphash v1.2.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/mroth/weightedrand v0.4.1 // indirect
	github.com/refraction-networking/utls v1.0.0 // indirect
	github.com/sergeyfrolov/bsbuffer v0.0.0-20180903213811-94e85abb8507 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	gitlab.com/yawning/edwards25519-extra.git v0.0.0-20211229043746-2f91fcc9fbdb // indirect
	gitlab.com/yawning/obfs4.git v0.0.0-20220204003609-77af0cba934d // indirect
	golang.org/x/crypto v0.0.0-20220516162934-403b01795ae8 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
